//
//  ContactViewController.swift
//  CarExhaustCalc
//

import UIKit

class ContactViewController: UIViewController, UITabBarDelegate {

    @IBOutlet weak var homeTabBar: UITabBar!
    @IBOutlet weak var homeTabBarItem: UITabBarItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        homeTabBar.delegate = self;
        // Do any additional setup after loading the view.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag == 2 {
            performSegue(withIdentifier: "showAbout", sender: self);
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
